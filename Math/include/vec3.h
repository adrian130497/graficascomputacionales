#pragma once
#pragma once
#include <iostream>

namespace cgmath
{
	class vec3
	{
	public:
		// Los atributos se almacenan de manera contigua en memoria. Pueden usar esto a su favor
		// para simplificar el codigo de algunas secciones.
		float x;
		float y;
		float z;

		vec3();

		vec3(float x, float y, float z);

		float& operator[](int i);

		const float& operator[](int i) const;

		vec3& operator*=(float s);

		vec3& operator/=(float s);

		vec3& operator+=(const vec3& v);

		vec3& operator-=(const vec3& v);

		const bool operator==(const vec3& v) const;

		float magnitude() const;

		void normalize();

		static float magnitude(const vec3& v) {
			return sqrt((v.x*v.x) + (v.y*v.y) + (v.z*v.z));
		}

		static vec3 normalize(const vec3& v) {
			vec3 normVec;
			normVec.x = v.x / magnitude(v);
			normVec.y = v.y / magnitude(v);
			normVec.z = v.z / magnitude(v);
			return normVec;
		}

		static float dot(const vec3& a, const vec3& b) {
			return (a.x * b.x + a.y *b.y + a.z*b.z);
		}

		static vec3 cross(const vec3& a, const vec3& b) {
			vec3 crossVec;
			crossVec.x = a.y*b.z - a.z*b.y;
			crossVec.y = a.z * b.x - a.x*b.z;
			crossVec.z = a.x*b.y - a.y*b.x;
			return crossVec;

		}

		friend std::ostream& operator<<(std::ostream& os, const vec3& v) {
			os << "(" << v.x << ", " << v.y << ", " << v.z << ")";
			return os;
		}
	};
	inline vec3 operator*(const vec3& v, float s) {
		return vec3(v.x* s, v.y*s, v.z*s);
	}

	inline vec3 operator*(float s, const vec3& v) {
		return vec3(v.x* s, v.y*s, v.z*s);
	}

	inline vec3 operator/(const vec3& v, float s) {
		return vec3(v.x / s, v.y / s, v.z/s);
	}

	inline vec3 operator+(const vec3& a, const vec3& b) {
		return vec3(a.x + b.x, a.y + b.y, a.z+b.z);
	}

	inline vec3 operator-(const vec3& a, const vec3& b) {
		return vec3(a.x - b.x, a.y - b.y, a.z - b.z);
	}

	inline vec3 operator-(const vec3& v) {
		vec3 nuevoVec;
		nuevoVec.x = v.x*-1;
		nuevoVec.y = v.y*-1;
		nuevoVec.z = v.z*-1;

		return nuevoVec;
	}


}