#pragma once
#include <iostream>
#include <vec3.h>

namespace cgmath
{
	class mat3 {
	private:

		float n[3][3] = { 0 };

	public:
		mat3();

		mat3(float diagonal);

		mat3(const vec3& a, const vec3& b, const vec3& c);

		vec3& operator[](int column);

		const vec3& operator[](int column) const;

		bool operator==(const mat3& m) const;

		static float determinant(const mat3& m) {
			return
				(m[0][0] * m[1][1] * m[2][2]) +
				(m[0][1] * m[1][2] * m[2][0]) +
				(m[0][2] * m[1][0] * m[2][1]) -
				(m[0][2] * m[1][1] * m[2][0]) -
				(m[0][0] * m[1][2] * m[2][1]) -
				(m[0][1] * m[1][0] * m[2][2]);
		}

		

		static mat3 inverse(const mat3& m) {

			mat3 inversa;
			const cgmath::vec3& x = m[0];
			const cgmath::vec3& y = m[1];
			const cgmath::vec3& z = m[2];

			cgmath::vec3 r1 = cgmath::vec3::cross(y, z);
			cgmath::vec3 r2 = cgmath::vec3::cross(z, x);
			cgmath::vec3 r3 = cgmath::vec3::cross(x, y);

			float detInversa = 1.0f / cgmath::vec3::dot(x, r1);

			r1 *= detInversa;
			r2 *= detInversa;
			r3 *= detInversa;

			inversa[0][0] = r1.x;
			inversa[0][1] = r2.x;
			inversa[0][2] = r3.x;

			inversa[1][0] = r1.y; 
			inversa[1][1] = r2.y;
			inversa[1][2] = r3.y;

			inversa[2][0] = r1.z;
			inversa[2][1] = r2.z;
			inversa[2][2] = r3.z;

			return inversa;
		}

		static mat3 transpose(const mat3& m) {
			return mat3(
				vec3(m[0][0], m[1][0], m[2][0]),
				vec3(m[0][1], m[1][1], m[2][1]),
				vec3(m[0][2], m[1][2], m[2][2]));
		}

		friend std::ostream& operator<<(std::ostream& os, const mat3& m) {
			for (int i = 0; i <=2; i++) {
				for (int j = 0; j <=2; j++) {
					os << m[j][i]; if (j < 2) os << " ";
				}if (i <2) os << "\n";
			}
			return os;
		}

	};

	inline vec3 operator*(const mat3&  m, const vec3&  v) {
		return vec3 (
			m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
			m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
			m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);

	}

	inline mat3 operator*(const mat3& m1, const mat3& m2) {
		return mat3( 
			vec3(
			m1[0][0] * m2[0][0] + m1[1][0] * m2[0][1] + m1[2][0] * m2[0][2],
			m1[0][0] * m2[1][0] + m1[1][0] * m2[1][1] + m1[2][0] * m2[1][2],
			m1[0][0] * m2[2][0] + m1[1][0] * m2[2][1] + m1[2][0] * m2[2][2]),
			vec3(
			m1[0][1] * m2[0][0] + m1[1][1] * m2[0][1] + m1[2][1] * m2[0][2],
			m1[0][1] * m2[1][0] + m1[1][1] * m2[1][1] + m1[2][1] * m2[1][2],
			m1[0][1] * m2[2][0] + m1[1][1] * m2[2][1] + m1[2][1] * m2[2][2]),
			vec3(
			m1[0][2] * m2[0][0] + m1[1][2] * m2[0][1] + m1[2][2] * m2[0][2],
			m1[0][2] * m2[1][0] + m1[1][2] * m2[1][1] + m1[2][2] * m2[1][2],
			m1[0][2] * m2[2][0] + m1[1][2] * m2[2][1] + m1[2][2] * m2[2][2]));
	}

}
