#include "vec2.h"
#include <cmath>


cgmath::vec2::vec2() : x(0.0f), y(0.0f)
{
}

cgmath::vec2::vec2(float x, float y) : x(x), y(y)
{
}

float & cgmath::vec2::operator[](int i)
{
	return (&x)[i];
}

const float & cgmath::vec2::operator[](int i) const
{
	return (&x)[i];

}

cgmath::vec2& cgmath::vec2::operator*=(float s)
{
	x = x * s;
	y = y * s;

	return *this;
}

cgmath::vec2& cgmath::vec2::operator/=(float s)
{
	x = x / s;
	y = y / s;

	return *this;
}

cgmath::vec2& cgmath::vec2::operator+=(const vec2 & v)
{
	x = x + v.x;
	y = y + v.y;

	return *this;
}

cgmath::vec2& cgmath::vec2::operator-=(const vec2 & v)
{
	x = x - v.x;
	y = y - v.y;

	return *this;
}

const bool cgmath::vec2::operator==(const vec2 & v) const
{
	if (x == v.x && y == v.y){
		return true;
	}
	else{
		return false;
	}
}

float cgmath::vec2::magnitude() const
{
	return sqrt((x*x)+(y*y));
}

void cgmath::vec2::normalize()
{
	float m = magnitude();
	x = x / m;
	y = y / m;
}












