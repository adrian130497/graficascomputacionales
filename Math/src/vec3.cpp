#include "vec3.h"
#include <cmath>


cgmath::vec3::vec3() : x(0.0f), y(0.0f), z(0.0f)
{
}

cgmath::vec3::vec3(float x, float y,float z) : x(x), y(y), z(z)
{
}

float & cgmath::vec3::operator[](int i)
{
	return (&x)[i];
}

const float & cgmath::vec3::operator[](int i) const
{
	return (&x)[i];

}

cgmath::vec3& cgmath::vec3::operator*=(float s)
{
	x = x * s;
	y = y * s;
	z = z * s;

	return *this;
}

cgmath::vec3& cgmath::vec3::operator/=(float s)
{
	x = x / s;
	y = y / s;
	z = z / s;

	return *this;
}

cgmath::vec3& cgmath::vec3::operator+=(const vec3 & v)
{
	x = x + v.x;
	y = y + v.y;
	z = z + v.z;

	return *this;
}

cgmath::vec3& cgmath::vec3::operator-=(const vec3 & v)
{
	x = x - v.x;
	y = y - v.y;
	z = z - v.z;

	return *this;
}

const bool cgmath::vec3::operator==(const vec3 & v) const
{
	if (x == v.x && y == v.y && z==v.z) {
		return true;
	}
	else {
		return false;
	}
}

float cgmath::vec3::magnitude() const
{
	return sqrt((x*x) + (y*y)+ (z*z));
}

void cgmath::vec3::normalize()
{
	float m = magnitude();
	x = x / m;
	y = y / m;
	z = z / m;
}












