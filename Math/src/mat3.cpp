#include "mat3.h"
#include "vec3.h"
#include <cmath>
#include <iostream>

cgmath::mat3::mat3(){
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			n[i][j] = 0;
}

cgmath::mat3::mat3(float diagonal) {
	
	n[0][0] = diagonal;
	n[1][1] = diagonal;
	n[2][2] = diagonal;

}

cgmath::mat3::mat3(const vec3 & a, const vec3 & b, const vec3 & c)
{
	reinterpret_cast<vec3 &>(n[0])=a;
	reinterpret_cast<vec3 &>(n[1]) =b;
	reinterpret_cast<vec3 &>(n[2]) = c;

}

cgmath::vec3& cgmath::mat3::operator[](int column)
{
	return reinterpret_cast<vec3 &>(n[column]);
}

const cgmath::vec3 & cgmath::mat3::operator[](int column) const
{
	return reinterpret_cast<const vec3 &>(n[column]);
}

bool cgmath::mat3::operator==(const mat3 & m) const
{
	for (int i = 0; i <= 2; i++) {
		for (int j = 0; j <= 2; j++) {
			if (n[i][j] != m[i][j]) {
				return false;
			}return true;
		}
	}

}




