#pragma once
#include <GL/glew.h>
#include "vec3.h"
#include "vec2.h"
#include <vector>

class Cub
{
public:
	void init();
	void Draw();

private:
	GLuint vaoCub;
	GLuint cubPos;
	GLuint pos;
	int size;
};