#include "scene.h"
#include "vec2.h"
#include <vector>

class scene_conchoid : public scene {
public:
	void init();
	void awake();
	void sleep();
	void reset();
	void mainLoop();
	void resize(int width, int height) {}
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) {}
	void specialKeys(int key) {}
	void passiveMotion(int x, int y) {}

private:
	std::vector<cgmath::vec2> linea;

	GLuint vaoPositiva;
	GLuint positionsVBOPositiva;

	GLuint vaoNegativa;
	GLuint positionsVBONegativa;

	GLenum primitiveType;

};