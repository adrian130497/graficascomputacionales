#pragma once
#include "Cub.h"
#include <GL/glut.h>
#include "vec3.h"
#include <vector>




class Particulas{
public:


	Cub cube;
	cgmath::vec3 pos;
	cgmath::vec3 vel;
	cgmath::vec3 acel;
	cgmath::vec3 camPos;
	float tiempoVid;

	void init(cgmath::vec3 positions, cgmath::vec3 velocity, cgmath::vec3 aceleration, float tiempoVida);

	void update();
	void draw(GLuint shaderID);
private:

};