#include "scene.h"
#include "vec2.h"
#include <vector>


class scene_chaikin : public scene {
public:
	void init();
	void awake();
	void sleep();
	void reset();
	void mainLoop();

	//void chaikin(std::vector< cgmath::vec2 > &posicion);
	std::vector<cgmath::vec2> chaikin(std::vector<cgmath::vec2> &puntos, int flag);
	void resize(int width, int height) {}
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) {}
	void specialKeys(int key) {}
	void passiveMotion(int x, int y) {}
	void original();
	void originalLoop();
	
	std::vector<std::vector<cgmath::vec2>> posicion = {
	//cabezaizquierda
	{{-0.78, -0.22879},{-0.45290, -1},{0,-1}},
	//CabezaArribaIzquiera
	{{-0.52187, -0.8349},{-0.72501, -0.37175},{0.02427, -0.27316}},
	//cabezaDerecha
	{{0.78, -0.22879},{0.45290, -1},{0,-1}},
	//CabezaArribaDerecha
	{{0.52187, -0.8349},{0.72501, -0.37175},{0.02427, -0.27316}},
	//CabezaHongo
	{{-0.23111, -0.804436},{-1, -0.43091},{-1, -0.03655}, {-0.80783, 0.49978},{-.81834,0.50},{-0.80783, 0.49978},{-0.47225,0.95318},{0.00850, 0.95330},
	{0.47225,0.95318},{0.80783, 0.49978},{.81834,0.50}, {0.80783, 0.49978}, {1, -0.03655},{1, -0.43091},{0.61065, -0.62809}},
	//Izquierda1Hongo
	//{{-0.61065, -0.62809},{-1, -0.43091},{-1, -0.03655}},
	//Izquierda2Hondo
	//{{-1, -0.03655},{-1,0.26143},{-0.80783, 0.49978}},
	//Izquierda3Hongo
	//{{-0.80783, 0.49978},{-0.47225,0.95318},{0.00850, 0.95330}},
	//Derecha3Hongo
	//{{0.00850, 0.95330},{0.47225,0.95318},{0.80783, 0.49978}},
	//Derecha2Hongo
	//{{0.80783, 0.49978},{1,0.26143}, {0.80783, 0.49978}},
	//Derecha1Hongo
	//{{1, -0.03655},{1, -0.43091},{0.61065, -0.62809}},
	//ManchaIzquierda
	{ {-1.12987, -0.37645},{-0.85451,-0.14429},{-0.76609, 0.06222},{-0.75135,0.27197},{-0.79284, 0.51}},
	//ManchaDerecha
	{ {1.12987, -0.37645},{0.85451,-0.14429},{0.76609, 0.06222},{0.75135,0.27197},{0.79284, 0.51}},
	//ManchaEnmedio
	{{-0.00727, -0.03655},{-0.23111, 0.00872},{-0.40766, 0.16921},{-0.48790, 0.37250},{-0.42529, 0.65358},{-0.26321, 0.84328},{0.00850, 0.95330},
	{0.26321, 0.84328},{0.42529, 0.65358},{0.48790, 0.37250},{0.40766, 0.16921},{0.23111, 0.00872},{0.00727, -0.03655}},
	//Ojoizquierdo
	{{-0.19167, -0.70832},{-0.23675, -0.69472},{-0.24787, -0.55663},{-0.23675, -0.40650},{-0.19782, -0.389819},
	{-0.16138, -0.409024},{-0.14802, -0.55778},{-0.16138, -0.69670}},
	//OjoDerecho
	{{0.19167, -0.70832},{0.23675, -0.69472},{0.24787, -0.55663},{0.23675, -0.40650},{0.19782, -0.389819},
	{0.16138, -0.409024},{0.14802, -0.55778},{0.16138, -0.69670}}
	};

	std::vector<std::vector<cgmath::vec2>> toad;
	std::vector<std::vector<std::vector<cgmath::vec2>>> toadref;


private:
	GLuint vao[10];
	GLuint positionsVBO[10];

	GLuint vao1[20];
	GLuint positionsVBO1[20];
	GLenum primitiveType;
	GLenum primitive;


};