#pragma once
#include "scene.h"
#include "SistemaParticulas.h"
#include "Particulas.h"

class scene_lluvia : public scene
{
public:
	SistemaParticulas sistemaPart;

	cgmath::vec3 camPos;

	cgmath::vec3 minPos, maxPos, minVel, maxVel, acel;
	float minTiempoV, maxTiempoV;

	void init();
	void awake();
	void sleep();
	void reset() { }
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) { }
	void specialKeys(int key) { }
	void passiveMotion(int x, int y) { }

private:

	GLuint textureID;

	GLuint shaderID;

	float aspect=1.0f;
};
