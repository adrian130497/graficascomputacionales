#pragma once
#include "Particulas.h"
#include <vector>
#include <random>


class SistemaParticulas{
public:

	std::default_random_engine rangen;
	std::uniform_real_distribution<float> distribution{ 0.0,1.0 };

	std::vector<Particulas> particulas;

	cgmath::vec3 minPos, maxPos,minVel, maxVel, acel;
	float minTiempoV, maxTiempoV, velocidadF;

	void init(cgmath::vec3 minPosition, cgmath::vec3 maxPosition, cgmath::vec3 minVelocity, cgmath::vec3 maxVelocity,
		float minTiempoVida, float maxTiempoVida,  cgmath::vec3 aceleration);

	void update(cgmath::vec3 camPosition, float aceleracion);
	void draw(GLuint shaderID);

};
