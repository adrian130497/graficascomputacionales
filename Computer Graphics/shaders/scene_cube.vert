#version 330

in vec3 VertexPosition;
in vec3 Color;

uniform mat4 mvpMatrix;


out vec4 InterpolatedColor;

void main(){
	vec4 pos = vec4(VertexPosition, 1.0f);
	
	InterpolatedColor = vec4(Color, 1.0f); 

	gl_Position = mvpMatrix * pos;
}
