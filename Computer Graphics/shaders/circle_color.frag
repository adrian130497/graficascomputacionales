#version 330

in vec4 InterpolatedColor;
out vec4 FragColor;
uniform vec2 iResolution;

void main(){
	
	vec2 pointPos= gl_FragCoord.xy / iResolution;
	vec2 center= vec2(0.5, 0.5);

	float ratio = length(pointPos-center);


	if(ratio < 0.25f){
		discard;
	}

	FragColor = InterpolatedColor;
}
