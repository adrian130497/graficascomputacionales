#version 330

out vec3 InterpolatedColor;

uniform float time;

vec2 circlePoints(float id, float segments)
{
  float x = floor(gl_VertexID/ 2.0f) + mod(gl_VertexID, 2.0f) ;
  float y = mod(floor(gl_VertexID / 2.0f) + floor(gl_VertexID / 3.0f), 2.0f);
  
  float angle = x / segments * radians(360.0f);
  float radius = 2.0f - y;
  
  float u = radius * cos(angle);
  float v = radius * sin(angle);
  
  return vec2(u, v);

}

void main()
{
  float puntos = 120.0f;

  float circleNum = floor(gl_VertexID / puntos);
  
  float width = 6.0f;
  
  float x = mod(circleNum, width);   
  float y = floor(circleNum / width); 
  
  float u = x / (width - 1.0f);
  float v = y / (width - 1.0f);
  
  float xOffset = cos(time + y * 0.2f) * 0.1f;
  float yOffset = cos(time + x * 0.3f) * 0.2f;
  
  float ux = u * 2.0f - 1.0f + xOffset;
  float vy = v * 2.0f - 1.0f + yOffset - 0.9;

  vec2 xy = circlePoints(gl_VertexID, 20.0) * 0.095f + vec2(ux, vy);
  
  gl_Position = vec4(xy, 0.0f, 2.3f);
  InterpolatedColor = vec3(0.0f, 0.6f, 1.0f);
}