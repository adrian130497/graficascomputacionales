#version 330

in vec3 VertexPos;
in vec2 VertexTextCoord;

out vec2 InterpolatedTexCoord;

uniform mat4 matModelo;
uniform mat4 matVista;
uniform mat4 matProyeccion;

void main()
{
	InterpolatedTexCoord = VertexTextCoord;
	mat4 matModelV = matVista * matModelo;

	matModelV[0][0] =1;
	matModelV[0][1] =0;
	matModelV[0][2] =0;

	matModelV[1][0] = 0;
	matModelV[1][1] = 1;
	matModelV[1][2] = 0;

	matModelV[2][0] = 0;
	matModelV[2][1] = 0;
	matModelV[2][2] = 1;

	gl_Position = matProyeccion * matModelV * vec4(VertexPos, 1.0f);
}