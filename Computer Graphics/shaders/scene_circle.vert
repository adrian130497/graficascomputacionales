#version 330

out vec4 InterpolatedColor;
in vec3 VertexPosition;
in vec3 Color;

void main(){
	gl_Position = vec4(VertexPosition, 1.0f);
	InterpolatedColor = vec4(Color, 1.0f);

}