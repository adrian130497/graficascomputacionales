#version 330

in vec4 InterpolatedColor;

out vec4 FragColor;

void main()
{
	FragColor = InterpolatedColor;
}