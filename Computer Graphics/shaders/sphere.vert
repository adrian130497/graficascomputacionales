#version 330 core

out vec3 InterpolatedColor;

uniform float time;

void main()
{
  float medioCir= 180.0;
  float completoCir= 360.0;

  float angulo = radians(mod(gl_VertexID * 5.0, medioCir));
  float angulo2 = radians(mod(floor(gl_VertexID / (medioCir / 5.0)) * 5.0, completoCir));
  
  float x = sin(angulo) * cos(angulo2);
  float y = sin(angulo) * sin(angulo2);
  float z = cos(angulo);
  
  vec3 newVec = vec3(x, y, z);

  mat4 matZ = mat4(1.0);
  matZ[0][0] = cos(time);
  matZ[1][0] = -sin(time);
  matZ[0][1] = sin(time);
  matZ[1][1] = cos(time);
  
  mat4 matX = mat4(1.0);
  matX[1][1] = cos(time);
  matX[2][1] = -sin(time);
  matX[1][2] = sin(time);
  matX[2][2] = cos(time);
  
  gl_Position = matZ * matX * vec4(newVec, 1.0);
  gl_PointSize = 3.0;
  InterpolatedColor = vec3(0.0, 0.6, 1.0);
}