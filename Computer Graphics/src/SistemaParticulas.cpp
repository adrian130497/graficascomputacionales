#include "SistemaParticulas.h"
#include "Particulas.h"
#include <algorithm>


void SistemaParticulas::init(cgmath::vec3 minPosition, cgmath::vec3 maxPosition, cgmath::vec3 minVelocity, cgmath::vec3 maxVelocity,
	float minTiempoVida, float maxTiempoVida, cgmath::vec3 aceleration)
{
	minPos = minPosition;
	maxPos = maxPosition;
	minVel = minVelocity;
	maxVel = maxVelocity;
	minTiempoV = minTiempoVida;
	maxTiempoV = maxTiempoVida;	
	acel = aceleration;

	//Al iniciarse 
	for (int i = 0; i <= 1400; i++){
		
		//Posicion
		float randX = distribution(rangen) * (maxPos.x - minPos.x) + minPos.x;
		float randY = distribution(rangen) * (maxPos.y - minPos.y) + minPos.y;
		float randZ = distribution(rangen) * (maxPos.z - minPos.z) + minPos.z;
		cgmath::vec3 positions(randX, randY, randZ);


		//Velocidad
		float randXVel = distribution(rangen) * (maxVel.x - minVel.x) + minVel.x;
		float randYVel = distribution(rangen) * (maxVel.y - minVel.y) + minVel.y;
		float randZVel = distribution(rangen) * (maxVel.z - minVel.z) + minVel.z;
		cgmath::vec3 velocity(randXVel, randYVel, randZVel);

		//Tiempo de Vida
		float tiempoV = distribution(rangen) * (maxTiempoV - minTiempoV) + minTiempoV;


		particulas.push_back(Particulas());

		//Se inician con sus atributos
		particulas[i].init(positions, velocity, acel, tiempoV);

	}
}

void SistemaParticulas::update(cgmath::vec3 camPosition, float aceleracion)
{
	//En cada Update
	for (int i = 0; i <= 1400; i++){

		//Posicion
		float randomX = distribution(rangen) * (maxPos.x - minPos.x) + minPos.x;
		float randomY = distribution(rangen) * (maxPos.y - minPos.y) + minPos.y;
		float randomZ = distribution(rangen) * (maxPos.z - minPos.z) + minPos.z;
		cgmath::vec3 positions(randomX, randomY, randomZ);

		//Velocidad
		float randomXV = distribution(rangen) * (maxVel.x - minVel.x) + minVel.x;
		float randomYV = distribution(rangen) * (maxVel.y - minVel.y) + minVel.y;
		float randomZV = distribution(rangen) * (maxVel.z - minVel.z) + minVel.z;
		cgmath::vec3 velocity(randomXV, randomYV, randomZV);

		//Tiempo de Vida
		float tiempoV = distribution(rangen) * (maxTiempoV - minTiempoV) + minTiempoV;

		
		//Checa si aun viven en el ambiente
		if (particulas[i].tiempoVid <= 0) {
			particulas[i].init(positions, velocity, acel, tiempoV);
		}

		//Se actualizan con sus atributos 
		particulas[i].update();
		particulas[i].camPos = camPosition;

	}
}

bool drawUtil(Particulas i, Particulas j)
{
	cgmath::vec3 distancia = i.camPos - i.pos;
	cgmath::vec3 distancia2 = j.camPos - j.pos;
	return distancia.magnitude() < distancia2.magnitude();
}

void SistemaParticulas::draw(GLuint shaderID)
{
	
	//Hace el sort y las dibuja
	std::sort(particulas.begin(), particulas.end(), drawUtil);
	for (int i = 0; i <= 1400; i++){
		particulas[i].draw(shaderID);
	}
}