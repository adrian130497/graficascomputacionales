#include "scene_conchoid.h"
#include "vec2.h"
#include <vector>
#include <math.h>


float a = .1;
float b = .3;

void scene_conchoid::init() {

	
	for (float i = -0.2; i <= 0.4; i += 0.01) {
		float y = sqrt(((pow(b, 2) * pow(i, 2)) / (pow(i - a, 2))) - pow(i, 2));
		float x = i / 2;
		y = y / 2;
		linea.push_back(cgmath::vec2(x, y));
	}

	for (float i = 0.4; i > -0.2; i -= 0.01) {
		float y = -sqrt(((pow(b, 2) * pow(i, 2)) / (pow(i - a, 2))) - pow(i, 2));
		float x = i / 2;
		y = y / 2;
		linea.push_back(cgmath::vec2(x,y));
		
	}


	glGenVertexArrays(1, &vaoPositiva);
	glBindVertexArray(vaoPositiva);
	glGenBuffers(1, &positionsVBOPositiva);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBOPositiva);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(cgmath::vec2)* linea.size(),
		linea.data(),
		GL_DYNAMIC_DRAW);

	glEnableVertexAttribArray(0);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	primitiveType = GL_LINE_LOOP;



}

void scene_conchoid::awake()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glPointSize(.7f);
}

void scene_conchoid::sleep()
{
	glPointSize(.7f);

}

void scene_conchoid::reset()
{
}

void scene_conchoid::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindVertexArray(vaoPositiva);
	glDrawArrays(primitiveType, 0, linea.size());//
	glBindVertexArray(0);

}

void scene_conchoid::normalKeysDown(unsigned char key)
{
}



