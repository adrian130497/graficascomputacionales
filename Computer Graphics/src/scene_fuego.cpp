#include "scene_fuego.h"
#include "cgmath.h"
#include "ifile.h"
#include "mat3.h"
#include "mat4.h"
#include "time.h"
#include "vec2.h"
#include "vec3.h"
#include "vec4.h"
#include <IL/il.h>
#include <vector>
#include "SistemaParticulas.h"
#include "Particulas.h"	

void scene_fuego::init()
{
	//Se hace blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//Posicion de camara 
	camPos.z = 28;

	//Posiciones
	minPos.x = -0.05;
	maxPos.x = .07;
	minPos.y = -6.0;
	maxPos.y = -7.5;
	minPos.z = -2.0;
	maxPos.z = 2.0;

	//Velocidad
	minVel.x = -0.5;
	maxVel.x = 0.5;
	minVel.y = 0.0;
	maxVel.y = 0.5;
	minVel.z = 0.0;
	maxVel.z = 0.5;

	//Tiempo de Vida 
	minTiempoV = 0;
	maxTiempoV = 5;

	//Aceleracion
	acel.y = .981;

	//Se Inicializan las particulas con sus atributos 
	sistemaPart.init(minPos, maxPos, minVel, maxVel,
		minTiempoV, maxTiempoV, acel);

	//Textura de lluvia
	ILuint imageID;

	ilGenImages(1, &imageID);
	ilBindImage(imageID);
	ilLoadImage("texturas/fuego.png");

	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT),
		0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE), ilGetData());
	glBindTexture(GL_TEXTURE_2D, 0);

	ilBindImage(0);
	ilDeleteImages(1, &imageID);

	ifile shader_file;
	shader_file.read("shaders/Particulas.vert");
	std::string vertex_source = shader_file.get_contents();
	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);
	glCompileShader(vertex_shader);

	shader_file.read("shaders/Particulas.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);

	shaderID = glCreateProgram();
	glAttachShader(shaderID, vertex_shader);
	glAttachShader(shaderID, fragment_shader);

	glBindAttribLocation(shaderID, 0, "VertexPos");
	glLinkProgram(shaderID);

	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	glUseProgram(shaderID);

	GLint tex_location = glGetUniformLocation(shaderID, "Texture");
	glUniform1i(tex_location, 0);

	glUseProgram(0);

}

void scene_fuego::awake()
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

void scene_fuego::sleep()
{
}

void scene_fuego::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(shaderID);

	// Matriz de Vista
	cgmath::mat4 matVista(1.0f);
	matVista[3][0] = camPos.x;
	matVista[3][1] = camPos.y;
	matVista[3][2] = camPos.z;
	matVista = cgmath::mat4::inverse(matVista);

	// Matriz de Proyeccion
	float lejos = 1000.0f;
	float cerca = 1.0f;
	float fieldOfView = cgmath::radians(30.0f);

	cgmath::mat4 matriz_Proy;
	matriz_Proy[0][0] = 1.0f / (aspect * tan(fieldOfView / 2));
	matriz_Proy[1][1] = 1.0f / tan(fieldOfView / 2);
	matriz_Proy[2][2] = -((lejos + cerca) / (lejos - cerca));
	matriz_Proy[2][3] = -1.0f;
	matriz_Proy[3][2] = -((2 * lejos * cerca) / (lejos - cerca));
	matriz_Proy[3][3] = 1.0f;



	// MVP Matrix
	GLuint model_location = glGetUniformLocation(shaderID, "matVista");
	glUniformMatrix4fv(model_location, 1, GL_FALSE, &matVista[0][0]);

	GLuint normal_location = glGetUniformLocation(shaderID, "matProyeccion");
	glUniformMatrix4fv(normal_location, 1, GL_FALSE, &matriz_Proy[0][0]);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureID);

	sistemaPart.update(camPos, acel.y);
	sistemaPart.draw(shaderID);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);


	glUseProgram(0);
}

void scene_fuego::normalKeysDown(unsigned char key)
{
	switch (key) {
	case 'a':
		camPos.x = camPos.x - 0.1f;
		break;
	case 'd':
		camPos.x = camPos.x + 0.1f;
		break;
	case 'w':
		camPos.z = camPos.z - 0.1f;
		break;
	case 's':
		camPos.z = camPos.z + 0.1f;
		break;
	case 't':
		acel.y -= 10;
		sistemaPart.update(camPos, acel.y);
		break;
	

	
	}
}

void scene_fuego::resize(int width, int height)
{
	glViewport(0, 0, width, height);
	aspect = static_cast<float>(width) / static_cast<float>(height);
}