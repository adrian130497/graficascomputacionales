﻿#include "scene_circle.h"

#include "ifile.h"
#include "time.h"
#include <string>
#include <math.h>
#include <vector>
#include "vec2.h"
#include "vec3.h"
#include <iostream>


scene_circle::~scene_circle()
{
	glDeleteProgram(shader_program);
}

void scene_circle::init()
{
	std::vector<cgmath::vec2> cir;
	std::vector<cgmath::vec3> col;
	std::vector<unsigned int> ind;

	cir.push_back(cgmath::vec2(0.0, 0.0));

	int grado = 1;
	float radian = 3.1416f / 180.0f;

	for (int i = 0; i <362; i++) {
		float angulo = i * radian;
		float punto1 = 1 * cos(angulo);
		float punto2 = 1 * sin(angulo);

		if (i == 0 ) {
			col.push_back(cgmath::vec3(1,1,1));

		}
		else {
			col.push_back(cgmath::vec3(punto1 - punto2, punto2, punto1*punto2));

		}
		cir.push_back(cgmath::vec2(punto1, punto2));

		ind.push_back(i);

	}
	// Creacion y activacion del vao
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// POSICION
	glGenBuffers(1, &pos);
	glBindBuffer(GL_ARRAY_BUFFER, pos);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * cir.size(), cir.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//COLOR
	glGenBuffers(1, &color);
	glBindBuffer(GL_ARRAY_BUFFER, color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * col.size(), col.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// INDICE
	glGenBuffers(1, &indice);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indice);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * ind.size(), ind.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);

	
	ifile shader_file;
	// Posicion
	shader_file.read("shaders/scene_circle.vert");
	
	std::string vertex_source = shader_file.get_contents();
	
	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();
	
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	
	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);
	
	glCompileShader(vertex_shader);

	GLint vertex_compiled;
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &vertex_compiled);
	if (vertex_compiled != GL_TRUE)
	{
		GLint log_length;
		glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log;
		log.resize(log_length);
		glGetShaderInfoLog(vertex_shader, log_length, &log_length, &log[0]);
		std::cout << "Syntax errors in vertex shader: " << std::endl;
		for (auto& c : log) std::cout << c;
		std::cout << std::endl;
	}

	//COLOR
	shader_file.read("shaders/circle_color.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();
	

	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);

	GLint fragment_compiled;
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &fragment_compiled);
	if (fragment_compiled != GL_TRUE)
	{
		GLint log_length;
		glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log;
		log.resize(log_length);
		glGetShaderInfoLog(fragment_shader, log_length, &log_length, &log[0]);
		std::cout << "Syntax errors in fragment shader: " << std::endl;
		for (auto& c : log) std::cout << c;
		std::cout << std::endl;
	}

	
	shader_program = glCreateProgram();
	
	glAttachShader(shader_program, vertex_shader);
	glAttachShader(shader_program, fragment_shader);
	glBindAttribLocation(shader_program, 0, "VertexPosition");
	glBindAttribLocation(shader_program, 1, "Color");
	
	glLinkProgram(shader_program);

	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	

	glUseProgram(shader_program);
	GLuint resolution_location = glGetUniformLocation(shader_program, "iResolution");
	glUniform2f(resolution_location, 400.0f, 400.0f);
	glUseProgram(0);
}

void scene_circle::awake()
{
	glClearColor(1.0f, 1.0f, .5f, 1.0f);
	glEnable(GL_PROGRAM_POINT_SIZE);
}

void scene_circle::sleep()
{
	glClearColor(1.0f, 1.0f, 0.5f, 1.0f);
	glDisable(GL_PROGRAM_POINT_SIZE);
}

void scene_circle::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(shader_program);
	/*GLuint time_loc =
		glGetUniformLocation(shader_program, "time");
	glUniform1f(time_loc, time::elapsed_time().count());*/

	//glDrawArrays(GL_POINTS, 0, 5000);
	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLE_FAN, 362, GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);

	glUseProgram(0);
}

void scene_circle::resize(int width, int height)
{
	glViewport(0, 0, width, height);
}