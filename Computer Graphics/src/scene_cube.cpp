#include "scene_cube.h"


#include "ifile.h"
#include "time.h"
#include "vec3.h"
#include "mat4.h"
#include <math.h>

#include <string>
#include <vector>

#include <iostream>


scene_cube::~scene_cube()
{
	glDeleteProgram(shader_program);
}

void scene_cube::init()
{

	std::vector<cgmath::vec3> cube;
	std::vector<cgmath::vec3> col;
	std::vector<unsigned int> ind = { 0, 1, 2, 0, 2, 3, 4, 5, 6, 4, 6, 7, 8, 9, 10,
		8, 10, 11, 12, 13, 14, 12, 14, 15, 16, 17, 18, 16, 18, 19, 20,
		21, 22, 20, 22, 23 };

	float constant = 3.0f;
	
		//FRONT FACE
		cube.push_back(cgmath::vec3(-constant, -constant, constant));
		cube.push_back(cgmath::vec3(constant, -constant, constant));
		cube.push_back(cgmath::vec3(constant, constant, constant));
		cube.push_back(cgmath::vec3(-constant, constant, constant));
		//RIGHT FACE
		cube.push_back(cgmath::vec3(constant, -constant, constant));
		cube.push_back(cgmath::vec3(constant, -constant, -constant));
		cube.push_back(cgmath::vec3(constant, constant, -constant));
		cube.push_back(cgmath::vec3(constant, constant, constant));
		//BACK FACE
		cube.push_back(cgmath::vec3(constant, -constant, -constant));
		cube.push_back(cgmath::vec3(-constant, -constant, -constant));
		cube.push_back(cgmath::vec3(-constant, constant, -constant));
		cube.push_back(cgmath::vec3(constant, constant, -constant));
		//RIGHT FACE
		cube.push_back(cgmath::vec3(-constant, -constant, -constant));
		cube.push_back(cgmath::vec3(-constant, -constant, constant));
		cube.push_back(cgmath::vec3(-constant, constant, constant));
		cube.push_back(cgmath::vec3(-constant, constant, -constant));
		//TOP FACE
		cube.push_back(cgmath::vec3(-constant, constant, constant));
		cube.push_back(cgmath::vec3(constant, constant, constant));
		cube.push_back(cgmath::vec3(constant, constant, -constant));
		cube.push_back(cgmath::vec3(-constant, constant, -constant));
		//BOTTOM FACE
		cube.push_back(cgmath::vec3(-constant, -constant, -constant));
		cube.push_back(cgmath::vec3(constant, -constant, -constant));
		cube.push_back(cgmath::vec3(constant, -constant, constant));
		cube.push_back(cgmath::vec3(-constant, -constant, constant));

	//COLOR
	for (int i = 0; i < 4; i++)
		col.push_back(cgmath::vec3(0.0f, 0.4f, 1.0f));//blue
	for (int i = 0; i < 4; i++)
		col.push_back(cgmath::vec3(1.0f, 0.2f, 0.0f));//green
	for (int i = 0; i < 4; i++)
		col.push_back(cgmath::vec3(0.0f, 0.8f, 0.4f));//red
	for (int i = 0; i < 4; i++)
		col.push_back(cgmath::vec3(.6f, .4f, 0.2f));//yellow
	for (int i = 0; i < 4; i++)
		col.push_back(cgmath::vec3(0.8f, 0.2f, 1.0f));//purple
	for (int i = 0; i < 4; i++)
		col.push_back(cgmath::vec3(1.0f, 0.6f, 0.2f));//orange



	// Creacion y activacion del vao
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// POSICION
	glGenBuffers(1, &pos);
	glBindBuffer(GL_ARRAY_BUFFER, pos);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * cube.size(), cube.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//COLOR
	glGenBuffers(1, &color);
	glBindBuffer(GL_ARRAY_BUFFER, color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * col.size(), col.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// INDICE
	glGenBuffers(1, &indice);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indice);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * ind.size(), ind.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);


	ifile shader_file;
	// Posicion
	shader_file.read("shaders/scene_cube.vert");

	std::string vertex_source = shader_file.get_contents();

	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();

	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);

	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);

	glCompileShader(vertex_shader);

	GLint vertex_compiled;
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &vertex_compiled);
	if (vertex_compiled != GL_TRUE)
	{
		GLint log_length;
		glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log;
		log.resize(log_length);
		glGetShaderInfoLog(vertex_shader, log_length, &log_length, &log[0]);
		std::cout << "Syntax errors in vertex shader: " << std::endl;
		for (auto& c : log) std::cout << c;
		std::cout << std::endl;
	}

	//COLOR
	shader_file.read("shaders/scene_cube.frag");
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();


	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);

	GLint fragment_compiled;
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &fragment_compiled);
	if (fragment_compiled != GL_TRUE)
	{
		GLint log_length;
		glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log;
		log.resize(log_length);
		glGetShaderInfoLog(fragment_shader, log_length, &log_length, &log[0]);
		std::cout << "Syntax errors in fragment shader: " << std::endl;
		for (auto& c : log) std::cout << c;
		std::cout << std::endl;
	}


	shader_program = glCreateProgram();

	glAttachShader(shader_program, vertex_shader);
	glAttachShader(shader_program, fragment_shader);
	glBindAttribLocation(shader_program, 0, "VertexPosition");
	glBindAttribLocation(shader_program, 1, "Color");

	glLinkProgram(shader_program);

	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);



	glUseProgram(shader_program);
	GLuint resolution_location = glGetUniformLocation(shader_program, "iResolution");
	glUniform2f(resolution_location, 400.0f, 400.0f);
	glUseProgram(0);
}

void scene_cube::awake()
{
	glClearColor(1.0f, 1.0f, .5f, 1.0f);
	glEnable(GL_PROGRAM_POINT_SIZE);
}

void scene_cube::sleep()
{
	glClearColor(1.0f, 1.0f, 0.5f, 1.0f);
	glDisable(GL_PROGRAM_POINT_SIZE);
}

void scene_cube::mainLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(shader_program);
	float time = time::elapsed_time().count();

	//glDrawArrays(GL_POINTS, 0, 5000);
	float radians = (3.1416 / 180);
	float anguloX = time * radians * 30.0f;
	float anguloY = time * radians * 60.0f;
	float anguloZ = time * radians * 30.0f;

	//MATRIZ
	cgmath::vec4 rotacionxx(1.0f, 0.0f, 0.0f, 0.0f);
	cgmath::vec4 rotacionxy(0.0f, cos(anguloX), sin(anguloX), 0.0f);
	cgmath::vec4 rotacionxz(0.0f, -sin(anguloX), cos(anguloX), 0.0f);
	cgmath::vec4 rotacionxw(0.0f, 0.0f, 0.0f, 1.0f);
	cgmath::mat4 rotacionx(rotacionxx, rotacionxy, rotacionxz, rotacionxw);

	cgmath::vec4 rotacionyx(cos(anguloY), 0.0f, -sin(anguloY), 0.0f);
	cgmath::vec4 rotacionyy(0.0f, 1.0f, 0.0f, 0.0f);
	cgmath::vec4 rotacionyz(sin(anguloY), 0.0f, cos(anguloY), 0.0f);
	cgmath::vec4 rotacionyw(0.0f, 0.0f, 0.0f, 1.0f);
	cgmath::mat4 rotaciony(rotacionyx, rotacionyy, rotacionyz, rotacionyw);

	cgmath::vec4 rotacionzx(cos(anguloZ), sin(anguloZ), 0.0f, 0.0f);
	cgmath::vec4 rotacionzy(-sin(anguloZ), cos(anguloZ), 0.0f, 0.0f);
	cgmath::vec4 rotacionzz(0.0f, 0.0f, 1.0f, 0.0f);
	cgmath::vec4 rotacionzw(0.0f, 0.0f, 0.0f, 1.0f);
	cgmath::mat4 rotacionz(rotacionzx, rotacionzy, rotacionzz, rotacionzw);

	cgmath::mat4 matrix_Final = rotacionx * rotaciony * rotacionz;

	//MATRIZ DE VISTA
	cgmath::mat4 matriz_Vista(1.0f);
	matriz_Vista[3][2] = 10.0f;
	matriz_Vista = cgmath::mat4::inverse(matriz_Vista);

	//MATRIZ DE PROYECCION
	float far = 1000.0f;
	float near = 1.0f;
	float fieldOfView = radians * 60.0f;
	aspect = 1.0f;

	cgmath::mat4 matriz_Proy;
	matriz_Proy[0][0] = 1.0f / (aspect * tan(fieldOfView / 2));
	matriz_Proy[1][1] = 1.0f / tan(fieldOfView / 2);
	matriz_Proy[2][2] = -((far + near) / (far - near));
	matriz_Proy[2][3] = -1.0f;
	matriz_Proy[3][2] = -((2 * far * near) / (far - near));
	matriz_Proy[3][3] = 1.0f;

	//MATRIZ FINAL
	cgmath::mat4 Matriz_Final = matriz_Proy * matriz_Vista * matrix_Final;
	
	GLuint mvp = glGetUniformLocation(shader_program, "mvpMatrix");
	glUniformMatrix4fv(mvp, 1, GL_FALSE, &Matriz_Final[0][0]);


	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);

	glUseProgram(0);
}

void scene_cube::resize(int width, int height)
{
	glViewport(0, 0, width, height);
	aspect = static_cast<float>(width) / static_cast<float>(height);

}