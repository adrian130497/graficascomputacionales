#include "scene_primitives.h"

#include "vec2.h"

#include <vector>

//Se manda  llamar solo cuando inicia la aplicacion 
void scene_primitives::init() {
	std::vector<cgmath::vec2> positions;
	positions.push_back(cgmath::vec2(0.0f, 0.0f));
	positions.push_back(cgmath::vec2(-1.0f, 0.0f));
	positions.push_back(cgmath::vec2(0.0f, -0.4f));
	positions.push_back(cgmath::vec2(1.0f, 0.0f));

	//Crear un identificador para un vertex array object, Guarda ID en vao
	glGenVertexArrays(1, &vao);
	//empezar a trabajar con el siguiente vao
	glBindVertexArray(vao);
	//Identificador para un vertex buffer Object, Guarda ID en positionsVBO
	glGenBuffers(1, &positionsVBO);
	//Quiero trabajar con el buffer positionsVBO
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO);
	//Crea la memoria del buffer, especifica datos y la manda al GPU
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(cgmath::vec2)* positions.size(),
		positions.data(),
		GL_DYNAMIC_DRAW);

	//Prendo el atributo 0
	glEnableVertexAttribArray(0);

	//Configurar atributo 0, numero de componentes,Tipo de dato, normalizamos los datos,desfazamiento entre atributos
	//en la lista, Apuntador 
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	//Cuando se hace Bin con 0 se interpreta como unbind de positionsVBO
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbind del vao
	glBindVertexArray(0);
	primitiveType = GL_POINTS;
}

void scene_primitives::awake() {
	glClearColor(0.0f, 0.0f,0.0f, 1.0f);
	glPointSize(20.0f);
}

void scene_primitives::sleep() {
	glPointSize(2.0f);
}

void scene_primitives::reset()
{
}

void scene_primitives::mainLoop() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//Bind del vao que tiene todos los atributos 
	glBindVertexArray(vao);
	//llamada a dibujar tipo de primitica, desde donde, cuantos.
	glDrawArrays(primitiveType, 0, 4);
	//Unbind 
	glBindVertexArray(0);
}

void scene_primitives::normalKeysDown(unsigned char key) {

	if (key == '1') {
		primitiveType = GL_POINTS;
	}
	if (key == '2') {
		primitiveType = GL_LINE_STRIP;
	}
}