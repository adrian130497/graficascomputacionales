#include "scene_chaikin.h"

#include "vec2.h"
#include <vector>
#include <iostream>
/*Graacias a Erick Chavez por ayudarme con los errores al pintar las figuras */
int sinch = 0;
int con = 0;

void scene_chaikin::init() {
	//toadref.push_back(posicion);
	toad.push_back(chaikin(posicion[0], 0));
	toad.push_back(chaikin(posicion[1], 0));
	toad.push_back(chaikin(posicion[2], 0));
	toad.push_back(chaikin(posicion[3], 0));
	toad.push_back(chaikin(posicion[4], 0));
	toad.push_back(chaikin(posicion[5], 0));
	toad.push_back(chaikin(posicion[6], 0));
	toad.push_back(chaikin(posicion[7], 1));
	toad.push_back(chaikin(posicion[8], 1));
	toad.push_back(chaikin(posicion[9], 1));

	for (int i = 0; i < 12; i++) {
		
			toad[0]= chaikin(toad[0], 0);
			toad[1] = chaikin(toad[1], 0);
			toad[2] = chaikin(toad[2], 0);
			toad[3] = chaikin(toad[3], 0);
			toad[4] = chaikin(toad[4], 0);
			toad[5] = chaikin(toad[5], 0);
			toad[6] = chaikin(toad[6], 0);
			toad[7] = chaikin(toad[7], 1);
			toad[8] = chaikin(toad[8], 1);
			toad[9] = chaikin(toad[9], 1);

		}
	

		glGenVertexArrays(toad.size(), vao);//
		glGenBuffers(toad.size(), positionsVBO);//

		for (int i = 0; i < toad.size(); i++) {//
			glBindVertexArray(vao[i]);

			glBindBuffer(GL_ARRAY_BUFFER, positionsVBO[i]);
			glBufferData(GL_ARRAY_BUFFER,
				sizeof(cgmath::vec2) * toad[i].size(),///
				toad[i].data(),//
				GL_DYNAMIC_DRAW);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
			
		}
		

		glGenVertexArrays(posicion.size(), vao1);//
		glGenBuffers(posicion.size(), positionsVBO1);//


		for (int i = 0; i < posicion.size(); i++) {//
			glBindVertexArray(vao1[i]);

			glBindBuffer(GL_ARRAY_BUFFER, positionsVBO1[i]);
			glBufferData(GL_ARRAY_BUFFER,
				sizeof(cgmath::vec2) * posicion[i].size(),///
				posicion[i].data(),//
				GL_DYNAMIC_DRAW);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
			
		}
	
}


void scene_chaikin::awake() {
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glPointSize(.7f);
}

void scene_chaikin::sleep() {
	glPointSize(.7f);
}

void scene_chaikin::reset()
{
}

void scene_chaikin::mainLoop() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if (con) {
		for (int i = 0; i < toad.size(); i++) {//
			glBindVertexArray(vao[i]);
			glDrawArrays(GL_LINE_STRIP, 0, toad[i].size());//
			glBindVertexArray(0);
		}
	}
	if (sinch) {
		for (int i = 0; i < posicion.size(); i++) {//
			glBindVertexArray(vao1[i]);
			glDrawArrays(GL_LINES, 0, posicion[i].size());//
			glBindVertexArray(0);
		}
	}

}



std::vector<cgmath::vec2> scene_chaikin::chaikin(std::vector<cgmath::vec2> &puntos, int flag) {
	std::vector<cgmath::vec2> posicion_n;
	cgmath::vec2 a, b, siguiente, actual;
	float quarter = 0.25;
	float thquarter = 0.75;
	for (int i = 0; i < puntos.size()-1; i++) {
		actual = puntos[i];
		siguiente = puntos[i + 1];

		a = actual*thquarter + quarter*siguiente;
		b = actual*quarter + thquarter*siguiente;
		posicion_n.push_back(a);
		posicion_n.push_back(b);
	}
	if (flag==1) {
		posicion_n.push_back(posicion_n[0]);

	}
	else {
		posicion_n.push_back(puntos[puntos.size() - 1]);
	}
	return posicion_n;
}


void scene_chaikin::normalKeysDown(unsigned char key) {
	
	if (key == '2') {
		sinch = 1;
	}
	if (key == '3') {
		con = 1;
	}
	if (key == '4') {
		sinch = 0;
	}
	if (key == '5') {
		con = 0;
	}



}


