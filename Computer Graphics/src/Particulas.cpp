#include "Cub.h"
#include "vec3.h"
#include "vec4.h"
#include "mat4.h"
#include "cgmath.h"
#include <vector>
#include <GL/glut.h>
#include "Particulas.h"
#include "time.h"
#include "math.h"

void Particulas::init(cgmath::vec3 positions, cgmath::vec3 velocity, cgmath::vec3 aceleration, float tiempoVida)
{
	cube.init();
	pos = positions;
	vel = velocity;
	acel = aceleration;
	tiempoVid = tiempoVida;

}

void Particulas::update()
{
	//VELOCIDAD
	vel = vel + acel * time::delta_time().count(); 

	//POSICION
	pos = pos + vel * time::delta_time().count(); 

	//TIEMPO DE VIDA
	tiempoVid = tiempoVid - time::delta_time().count();

}

void Particulas::draw(GLuint shaderID)
{
	//Matriz de Traslacion 

	cgmath::vec4 rotzx(1.0f, 0.0f, 0.0f, 0.0f);
	cgmath::vec4 rotzy(0.0f, 1.0f, 0.0f, 0.0f);
	cgmath::vec4 rotzz(0.0f, 0.0f, 1.0f, 0.0f);
	cgmath::vec4 rotzw(pos.x, pos.y, pos.z, 1.0f);
	cgmath::mat4 modelMat(rotzx, rotzy, rotzz, rotzw);

	GLuint modMat = glGetUniformLocation(shaderID, "matModelo");
	glUniformMatrix4fv(modMat, 1, GL_FALSE, &modelMat[0][0]);


	cube.Draw();
}