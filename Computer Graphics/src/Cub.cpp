#include "Cub.h"
#include <math.h>
#include <GL/glut.h>
#include "vec3.h"
#include "vec2.h"
#include <vector>

void Cub::init()
{
	//Cuadrado
	std::vector<cgmath::vec3> posID;
	std::vector<cgmath::vec2> textureID;
	posID.push_back(cgmath::vec3(0.7, 0, 0));
	posID.push_back(cgmath::vec3(0.7, 0.7, 0));
	posID.push_back(cgmath::vec3(0, 0, 0));
	posID.push_back(cgmath::vec3(0, 0.7, 0));

	textureID.push_back(cgmath::vec2(1, 0));
	textureID.push_back(cgmath::vec2(1, 1));
	textureID.push_back(cgmath::vec2(0, 0));
	textureID.push_back(cgmath::vec2(0, 1));

	size = posID.size();

	glGenVertexArrays(1, &vaoCub);  
	glBindVertexArray(vaoCub);

	glGenBuffers(1, &cubPos); 
	glBindBuffer(GL_ARRAY_BUFFER, cubPos); 
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec3) * posID.size(),  
		posID.data(),
		GL_STATIC_DRAW);
	glEnableVertexAttribArray(0); 
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr); 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &pos); 
	glBindBuffer(GL_ARRAY_BUFFER, pos); 
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * textureID.size(),
		textureID.data(),
		GL_STATIC_DRAW);
	glEnableVertexAttribArray(1); 
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr); 
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

}

void Cub::Draw()
{
	glBindVertexArray(vaoCub);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, size);
	glBindVertexArray(0);
}
